<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PublicController extends AbstractController
{
  /**
   * @Route("/", name="public")
   */
  public function index()
  {
    return $this->render('public/index.html.twig', [
        'controller_name' => 'PublicController',
    ]);
  }

  /**
   * @Route("/test", name="test")
   */
  public function test()
  {
    return new Response("Test");
  }

}
