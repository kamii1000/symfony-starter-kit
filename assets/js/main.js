/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// import main css file which includes globally needed css e.g. bootstrap,etc
import '../css/main.scss';

// Set jQuery globally
const $ = require('jquery');
global.$ = global.jQuery = $;

// import pace.js
import 'pace-js/themes/blue/pace-theme-minimal.css';
const pace = require('pace-js');
pace.start();

// import bootstrap.js, no need to set this to a variable, just require it
require('bootstrap');


